---
title: Pequeno-almoço linuxeiro
description: Pequeno-almoço Linuxeiro, nos dias das actividades do Centro Linux
featured_image: ''
omit_header_text: false
type: page
---

# O que é um pequeno-almoço linuxeiro?

É o pequeno-almoço que só os linuxeiros mais matinais, tomam em comunidade antes de começarem as actividades para o dia no Centro Linux.

O local em que vamos tomar o pequeno almoço é na Padaria Portuguesa, do Campo dos Mártires da Pátria, mas estamos abertos a novas sugestões de local igualmente perto do Centro Linux e de acesso igualmente simples:

```
Campo Mártires da Pátria
Alameda de Santo António dos Capuchos nº6 6-A e 6-B
1150-261 Lisboa 
```

O pequeno almoço linuxeiro é há hora marcada na agenda de cada dia de actividades do Centro Linux.
