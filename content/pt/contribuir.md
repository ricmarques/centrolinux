---
title: Contribuir
description: Como contribuir para o Centro Linux
featured_image: ''
omit_header_text: false
type: page
menu: main
---

O Centro Linux é da comunidade para a comunidade, e como tal depende das contribuições da mesma para existir e funcionar bem.

O Centor Linux precisa de ti!

# Como podes contribuir?

Há várias formas de contribuir, especialmente, mas não exclusivamente:

* propor e dirigir actividades;
* voluntariar para apoiar a realização das actividades;
    - ajudar no planeamento e agendamento das actividades;
    - preparar materiais de promoção das actividades;
    - preparação e montagem do espaço;
    - desmontagem do espaço;
    - apoiar a recepção de participantes;


# Actividades da comunidade, para a comunidade:

Faz parte do conceito do Centro Linux, que as actividades sejam propostas, planeadas, organizadas e realizadas pela comunidade. Ainda estamos a tentar perceber qual é a melhor forma de abrir cada uma destas fases e para já a maior parte das actividades resultam de convites feitos a membros especificos da comunidade, ou de conversas com membros da comunidade sobre o Centro Linux. Contudo queremos que as propostas passem a ver quase sempre, ou mesmo sempre da comunidade e por isso propomos estes métodos para que sejam feitas propostas:

## Propostas de eventos via email:

De forma a não excluir ninguém disponibilizamos um método menos low tech para fazer propostas: envia a tua proposta para o endereço de email: '''actividade {at} centrolinux.pt'''

# Queres ser voluntário e contribuir no apoio aos eventos?

Então se não estás já envolvido na comunidade, enviar um email para o endereço: '''voluntario {at} centrolinux.pt'''

# Este site

Este site e o seu conteúdo tem uma [licença de software livre](https://gitlab.com/DiogoConstantino/centrolinux/-/blob/master/LICENSE), e podes ver o código ou contribuir para o melhorar [aqui](https://gitlab.com/DiogoConstantino/centrolinux/).
