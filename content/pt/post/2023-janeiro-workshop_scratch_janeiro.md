---
date: 2023-01-10
description: "Workshop de introduação ao Scratch"
featured_image: ""
tags: ["2023", "Janeiro", "Workshop", "Scratch"]
title: "Workshop de Introdução ao Scratch"
---

[![Cartaz](https://centrolinux.pt/images/2023.01-Workshop_Introducao_scratch/post.twitter.png )](https://osm.org/go/b5crq_xVM?layers=N)

## Workshop de Introdução ao Scratch

### Vamos aprender a criar um jogo?

Utilizando o ambiente de programação Scratch, podemos aprender a programar e criar os nossos próprios projetos. Aqui, criar um jogo é muito divertido!

### Objectivos do workshop:

 * Descobrir programação em ambiente visual Scratch;
 * Aprender a criar um jogo simples;
 *  Introdução aos conceitos de Pensamento Computacional.

### Quando?

Na tarde de Sábado dia 28 de Janeiro de 2023.

A agenda é a seguinte:

| Hora | Descrição |
| ----- | --------- |
| 15:30 | Abertura  |
| 15:35 | Apresentação do Makers in Little Lisbon |
| 15:40 | Apresentação do Centro Linux |
| 15:45 | Início do Workshop de Introdução ao Scratch |
| 18:30 | Fim do workshop de Introdução ao Scratch|

### O Scratch ...

... é a ferramenta ideal para os mais jóvens (mas não só), introduzirem-se à programação de uma forma divertida por permitir rápida e facilmente obter resultados visuais.

Com o [Scratch](https://scratch.mit.edu/) é fácil criar histórias, jogos e animações e partilhar o resultado com todo o mundo!

O Scratch é uma linguagem de programação educacional criada inicialmente no MIT Media Lab, e agora é gerido pela [Fundação Scratch](https://www.scratchfoundation.org/) e é ideal para crianças dos 8 aos 16.

<p><a href="https://scratch.mit.edu/about?wvideo=sucupcznsp"><img src="https://embed-ssl.wistia.com/deliveries/57d075bb56d31ca0c9cad444d66e492e.jpg?image_play_button_size=2x&amp;image_crop_resized=960x540&amp;image_play_button=1&amp;image_play_button_color=7f7f7fe0" style="width: 400px; height: 225px;" width="400" height="225"></a></p><p><a href="https://scratch.mit.edu/about?wvideo=sucupcznsp"></a></p>

Para aprenderem mais sobre o scratch, podem visitar [o seu site](https://scratch.mit.edu/about), que tem uma secção especial de informação dedicada a [pais](https://scratch.mit.edu/parents/) e outra para [educadores](https://scratch.mit.edu/parents/).


### O nosso estimado formador convidado

É o Artur Coelho, um Professor de TIC no Agrupamento de Escolas Venda do Pinheiro. Formador ([ANPRI](https://anpri.pt) e [CFAERC](https://cfaerc.esjs-mafra.net/)). Membro da [Associação Lab Aberto Fab Lab](https://lababerto.pt/). O seu website é pode ser encontrado em [3dalpha.blogspot.com](http://3dalpha.blogspot.com/).


### Material necessário

O Centro Linux tem algum do material necessário que disponibilizaremos para os participantes, contudo podemos não ter material para todos dependendo da quantidade de participantes. Assim sendo, se for possível recomendamos que tragam um computador portátil.

### Local:

MILL – Makers In Little Lisbon
Calçada do Moinho de Vento, 14B,
1150-236 Lisboa

coordenadas: 38.72045,-9.14131?z=19

Localização do MILL no mapa do [OpenStreetMap](https://osm.org/go/b5crq_xVM?layers=N):

[![Localização do MILL](https://centrolinux.pt/images/map.png "mapa com a localização do MILL, clique para mais detalhes e navegação")](https://osm.org/go/b5crq_xVM?layers=N)

### Inscrições

**As Inscrições estão encerradas.**



Estamos a contar que ***apenas um dos pais permaneça no MILL*** e reforçamos que durante a aquisição do bilhete gratuito deve reflectir ***o mínimo de um dos pais e um menor são dois lugares***.

### Código de Conduta

O Centro Linux é organizado e promovido pela Comunidade Ubuntu-pt (sem que esteja limitado a participações e contribuições da mesma), que se rege pelo [Código de Conduta](https://ubuntu.com/community/code-of-conduct) e pela [declaração de diversidade do Ubuntu](https://ubuntu.com/community/diversity), adicionalmente por estar alojado no [Makers in Little Lisbon](https://mill.pt/), pelo qual estamos muito gratos e que por isso queremos garantir a melhor condição e ambiente possível nesse espaço.

### Questões e informações adicionais:

1. Os pais devem permanecer no local durante o Workshop?
Sim, o Centro Linux e o MILL não podem assumir responsabilidade pelos menores e por isso os pais devem permanecer. Além do mais poderá ser uma actividade interessante para eles.

2. Já não sou menor, nem tenho entre os 8 e 16 anos, posso participar?
Desde que não tenha qualquer inibição legal a estar junto de menores pode participar.
O Scratch é especialmente indicado para pessoas daquelas idades, mas não é de forma alguma limitado a elas. Contudo crianças mais novas do que oito anos poderão ainda não estar aptas a beneficiar do Scratch e desta actividade.

3. Quantos pais devem comparecer?
Deve comparecer e permanecer apenas um dos pais. Os motivos são simples:
* o espaço não é infinito, nem a capacidade de alojar todos de forma confortável;
* queremos manter o espaço o mais calmo possível para a actividade e para os restantes utilizadores do MILL;

4. Que software vou precisar de ter no computador?
O Scratch funciona via web. Apenas irá precisar de ter um qualquer sistema operativo com um navegador web moderno a funcionar.

5. Não tens um computador portátil?
Não é problema!
Temos alguns computadores que estarão disponíveis para os participantes, mas tens que nos informar antecipadamente para o email: reservas.scratch {at} centrolinux.pt

6. Tem mais questões sobre o workshop?
Se tiver quaisquer questões relativas ao workshop, pode contactar-nos via email usando o endereço: duvidas.scratch {at} centrolinux.pt

7. Tem mais questões sobre o Centro Linux?
Se tiver quaisquer questões relativas ao workshop, pode contactar-nos via email usando o endereço: duvidas {at} centrolinux.pt