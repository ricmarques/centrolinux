---
date: 2024-11-17
description: "Festa de lançamento do Ubuntu 24.10 Oracular Oriole, Workshop de streaming de vídeo, e Criar um Homelab com Microcloud"
featured_image: ""
tags: ["Linux", "2024", "Novembro", "24.10", "Oracular", "OracularOriole", "Ubuntu", "Release", "Streaming", "OBS", "Youtube", "Twitch", "HomeLab", "MicroCloud", "LXD", "Ceph", "OVN"]
title: "Festa de Lançamento do Ubuntu 24.10 Oracular Oriole"
---

[![Cartaz](https://centrolinux.pt/images/2024-06.Junho/cartaz.png)](https://osm.org/go/b5crq_xVM?layers=N)

Assinalou-se faz algumas semanas os **20 anos do Ubuntu**, data de lançamento do Ubuntu 4.10 Warty Warthog. Mas também tivemos recentemente o **lançamento da mais recente versão do Ubuntu o 24.10 Oracular Oriole**, e isso são **dois motivos para FESTA**!!!!!

# Descrições das actividades do evento:

## Instalações e experimentação do Ubuntu 24.10 Oracular Oriole

Nesta actividade vamos instalar e experimentar o Ubuntu 24.10 Oracular Oriole, nos computadores do laboratório do Centro Linux e se quiserem trazer os vossos computadores, também vamos poder experimentar sem instalar, ou até instalar.

O objectivo é dar a oportunidade de experimentar o Ubuntu, que é a mais popular distribuição de GNU/Linux, em especial nesta versão mais recente, bem como assistir a instalações da versão 24.10 ou de versões LTS de Ubuntu, a quem tenha esse desejo.

## Workshop de streaming de vídeo

Fazer streaming/transmissão de de vídeo é algo comum, muitos gostam de fazer enquanto jogam jogos, alguns enquanto fazem outras coisas como programar, ou para outros fins lúdicos, educativos, de difusão de informação jornalística, e um sem fim de propósitos.

Esta é também uma actividade que pode ser feita até com alguma facilidade utilização uma distribuição de GNU/Linux, e o mais popular software de streaming de vídeo, o OBS Studio.

Neste workshop vamos mostrar como se pode instalar e utilizar OBS Studio, para transmitir vídeo para plataformas online (por exemplo youtube e twitch).

## Workshop de Homelab com Microcloud

Já iniciámos na nossa comunidade do Centro Linux o caminho do homelab, este será mais um troço do caminho, em que vamos explorar mais uma alternativa.
Neste evento vamos explorar o Microcloud. O Microcloud é uma solução criada pela Canonical para criar uma infraestrutura de cloud Open Source Software, escalável, que pode ir de um a cinquenta nós. Nela vamos encontrar um amigo conhecido o LXD, que já explorámos na actividades anteriores, mas que está combinado e integrado com soluções de armazenamento e de rede.



## Quando?

No **Sábado dia 30 de Novembro de 2024 a partir das 11:30**
Haverá interrupção das actividades para almoço.

### Plano (provisório) de actividades:

#### Agenda

##### Manhã

| Hora  | Descrição |
| ----- | --------- |
| 10:00 | Pequeno-almoço linuxeiro *([detalhes aqui](https://centrolinux.pt/pequeno-almoco-linuxeiro/))* |
| 11:00 | Chegada dos voluntários ao Centro Linux |
| 11:05 | Montagem do espaço para as actividades                                       |
| 11:30 | Apresentação do [MILL - Makers In Little Lisbon](https://mill.pt) e do Centro Linux |
| 11:45 | Instalações e experimentação do Ubuntu 24.10 Oracular Oriole |
| 13:00 | Interrupção para o almoço |

##### Tarde
| Hora  | Descrição |
| ----- | --------- |
| 15:00 | Recomeço das actividades |
| 16:00 | Workshop de streaming de vídeo |
| 17:00 | Workshop de Homelab com Microcloud |
| 19:30 | Desmontagem e fim das actividades |

##### Destalhes das actividades

### Local:

O Evento será realizado no Centro Linux que se localiza no Makers In Little Lisbon - MILL, que é em Lisboa, muito perto do Campo dos Mártires da Pátria.
Pode consultar [onde estamos](https://centrolinux.pt/ondeestamos/) para morada e informações detalhadas sobre como pode chegar ao MILL de muitas formas diferentes.

### Inscrições

As inscrições fazem-se na [página do evento no site do Makers in Little Lisbon](https://mill.pt/events/)

### Código de Conduta

O Centro Linux é organizado e promovido pela Comunidade Ubuntu-pt (sem que esteja limitado a participações e contribuições da mesma), que se rege pelo [Código de Conduta](https://ubuntu.com/community/code-of-conduct) e pela [declaração de diversidade do Ubuntu](https://ubuntu.com/community/diversity), adicionalmente por estar alojado no [Makers in Little Lisbon](https://mill.pt/), pelo qual estamos muito gratos e que por isso queremos garantir a melhor condição e ambiente possível nesse espaço.
