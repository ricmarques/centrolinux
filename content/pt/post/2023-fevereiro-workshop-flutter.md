---
date: 2023-01-29
description: "Workshop de introdução ao Flutter"
featured_image: ""
tags: ["2023", "Workshop", "Flutter", "Dart", "Desktop", "Ubuntu", "Linux", "Desenvolvimento", "Março", "Flutter-pt"]
title: "Workshop de Introdução ao Flutter"
---

[![Cartaz](https://centrolinux.pt/images/2023.02-workshop_introducao_flutter/post.base.png )](https://osm.org/go/b5crq_xVM?layers=N)


## Workshop de Introdução ao Flutter

### O Flutter ...

De acordo com a Wikipédia o [Flutter](https://en.wikipedia.org/wiki/Flutter_(software)) é um kit de desenvolvimento de Software multi-plataforma (Android, iOS, Linux, MacOS X, Windows, e Fushia), com interface gráfica, licenciado como [Software Livre](https://ansol.org/software-livre/)/Software de Código Aberto (Open Source Software), criado pelo Google.

O lema do Flutter é: «Constroi software para qualquer ecrã». Podem aprender mais sobre o Flutter no seu [sítio web oficial](https://flutter.dev/).

### Cria aplicações para qualquer ecrã, usando Ubuntu

O que podem aprender no Workshop?

1. Introdução ao Flutter & Dart
2. Criar um projecto. - A app de contador
3. Temas e o Yaru
4. Widgets de filho único e múltiplos filhos
5. Gestão de estado
6. Pacotes
7. Navegação
8. Utilização de rede
9. Testes
10. Criar a aplicação.

### Pré-requisitos

1. Conhecimento de programação.
    Como o foco destes workshop é o Flutter, é necessário que os participantes tenham já conhecimento de programação de programas de computador, porque não é possível incluir neste workshop uma introdução à programação.
2. Computador portátil, com Ubuntu instalado, ou pelo menos com uma distribuição de GNU/Linux onde possa correr flutter (ver links do ponto seguinte para mais detalhes sobre isso).
    Alternativamente o Centro Linux pode facilitar o acesso a um dos computadores do seu laboratório, mas isto é feito a pedido e para um número limitado de pedidos. Mais detalhes na secção de Material Necessário.
3. Ter o Flutter instalado.
    Pode saber como instalar em:
    * https://docs.flutter.dev/get-started/install
    * https://diddledani.com/getting-started-with-flutter-on-ubuntu/
    Os voluntários do Centro Linux vão estar durante a manhã do dia do evento, no Centro Linux a instalar Flutter nos computadores do Laboratório do Centor Linux. Se algum dos participantes necessitar de ajuda pode também vir ao Centro Linux durante a manhã e faremos o possível para ajudar com a instalação.
4. Ter o Visual Studio Code/Codium instalado.
    Este será o editor utilizado pelo instrutor durante o workshop. É livre de utilizar outro mas a capacidade do instrutor de o suportar poderá ser limitada.

### Quando?

Na **tarde de Sábado dia 18 de Março de 2023 a partir das 14:30**. Contudo as actividades no Centro Linux para esse dia começam às 10h.

| Hora  | Descrição                                                                    |
| ----- | ---------------------------------------------------------------------------- |
| 10:30 | Pequeno-almoço linuxeiro *([detalhes aqui](https://centrolinux.pt/pequeno-almoco-linuxeiro/))* |
| 12:00 | Chegada ao Centro Linux                                                      |
| 12:10 | Montagem do espaço para as actividades da parte da tarde                     |
| 12:15 | Instalação dos computadores do Laboratório do Centro Linux                   |
| 12:20 | Instalação de Flutter nos computadores do Centro Linux                       |
| 12:30 | Apoio à instalação de Flutter nos computadores dos participantes do Workshop |
| 13:15 | Interrupção para o almoço                                                    |
| ----- | ---------------------------------------------------------------------------- |
| 14:20 | **Chegada dos participantes ao Centro Linux para participação**              |
| 14:30 | **Início do Workshop**                                                       |
| 18:30 | Fim do Workshop                                                              |
| ----- | ---------------------------------------------------------------------------- |

### O instrutor: Filipe Barroso:

Palestrante internacional, organizador e promotor da comunidade. Não é o vosso programador usual, adora sair da sua zona de conforto e se não souberem onde ele está ele está provavelmente a organizar o próximo evento de tecnologia. É um organizador de comunidade para o Google Developer Group de Lisboa e para a [Flutter Portugal](https://flutter.pt/). Além de programar, o Filipe gosta de ler sobre Psicologia, e suportar a comunidade local, tentando sempre um impacto social.


### Material necessário

Será necessário um computador portátil

Alternativamente o Centro Linux pode facilitar o acesso a um dos computadores do seu laboratório, mas isto é feito a pedido e para um número limitado de pedidos.

O pedido de acesso a computador do Centro Linux deve ser feito no formulário de inscrição, ou por email para o endereço: reservas.flutter [ at ] centrolinux ponto pt.

### Local:

O Workshop será realizado no Centro Linux que se localiza no Markers In Little Lisbon - MILL, que é em Lisboa, muito perto do Campo dos Mártires da Pátria.
Pode consultar [onde estamos](https://centrolinux.pt/ondeestamos/) para morada e informações detalhadas sobre como pode chegar ao MILL de muitas formas diferentes.

### Inscrições

Podem inscrever-se na página de [inscrições para este evento](https://mill.pt/agenda/centro-linux-workshop-de-introducao-ao-flutter/) no website do MILL.

Assinalamos que é importante ter em conta as nossas [regras de funcionamento](https://centrolinux.pt/funcionamento/), incluíndo a nossa [política de privacidade](https://centrolinux.pt/privacidade/).

Para o esclarecimento de qualquer dúvida sobre as inscrições podem contactar-nos por email usando o endereço: reservas.flutter {at} centrolinux ponto pt.

### Código de Conduta

O Centro Linux é organizado e promovido pela Comunidade Ubuntu-pt (sem que esteja limitado a participações e contribuições da mesma), que se rege pelo [Código de Conduta](https://ubuntu.com/community/code-of-conduct) e pela [declaração de diversidade do Ubuntu](https://ubuntu.com/community/diversity), adicionalmente por estar alojado no [Makers in Little Lisbon](https://mill.pt/), pelo qual estamos muito gratos e que por isso queremos garantir a melhor condição e ambiente possível nesse espaço.

Recomendamos que sigam os [valores](https://flutter.dev/culture) da cultura da comunidade Flutter.